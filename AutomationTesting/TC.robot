*** Settings ***
Library  Selenium2Library

*** Variables ***
${Browser}  Firefox
${URL}  http://qa-interview.srcli.xyz/
${loginUrl}  http://qa-interview.srcli.xyz/login
${logoutUrl}  http://qa-interview.srcli.xyz/logout
${dataUrl}  http://qa-interview.srcli.xyz/data

*** Keywords ***
Open Website
    open browser  ${URL}  ${Browser}

Show Login Page
    go to  ${loginUrl}
    sleep  2s

Show Data Page
    go to  ${dataUrl}
    sleep  5s

Valid Fill Login Form
    input text  name:username  root
    input text  name:password  root123
    click element  //input[@type='submit']

Invalid Fill Login Form
    input text  name:username  root
    input text  name:password  root
    click element  //input[@type='submit']

Logout
    go to  ${logoutUrl}
    go to  ${URL}
    sleep  2s
    close browser

Valid Filter Data
    input text  name:start  2018-07-01
    input text  name:end  2018-07-05
    click element  //input[@type='submit']

Invalid Filter Data
    input text  name:start  2018-07-10
    input text  name:end  2018-07-05
    click element  //input[@type='submit']

Close Windows
    sleep  3s
    close browser

*** Test Cases ***
TC_Show_Main_Page
    Open Website

TC_Before Login
    Show Login Page

TC_Valid_Login
    Valid Fill Login Form
    Close Windows

TC_Invalid_Login
    Open Website
    Show Login Page
    Invalid Fill Login Form

TC_Logout
    Logout

TC_Show_Data_Before_Login
    Open Website
    Show Data Page
    Close Windows

TC_Show_Data_After_Login
    Open Website
    Show Login Page
    Valid Fill Login Form
    Show Data Page
    Close Windows

TC_Filter_Data
    Open Website
    Show Login Page
    Valid Fill Login Form
    Show Data Page
    Valid Filter Data
    Close Windows

TC_Invalid_Filter_Data
    Open Website
    Show Login Page
    Valid Fill Login Form
    Show Data Page
    Invalid Filter Data
    Close Windows