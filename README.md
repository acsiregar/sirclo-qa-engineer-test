# SIRCLO-QA Engineer Test

This Project contains:
1. The test scenario for Remote Control Testing
2. The automation test script for Financial Management System
	-test script is written using python with selenium library and robot framework
	-to run the test, you need to install python on your pc
	-after the python is installed, go to your command prompt and type the project directory
	-then type: robot TC.robot
	-the test will run automaticaly
